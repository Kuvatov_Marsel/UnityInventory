﻿using Source.Scripts.ScriptableObjects;
using UnityEngine;

namespace Source.Scripts.Slots
{
  public class ItemMovement:MonoBehaviour
  {
    
    [SerializeField] private InventorySlot[] _bottomSlots;
    [SerializeField] private InventorySlot[] _mainSlots;
    private ItemSpawner spawner;
    private InventorySlot currentActiveSlot;
    public void ActiveFirstSlot()
    {
      
      InventorySlot inventorySlot =  _bottomSlots[0].GetComponent<InventorySlot>();
      if(inventorySlot != null)
      {
        currentActiveSlot = inventorySlot;
        Debug.Log("InventorySlot");
        inventorySlot.ChangeActive(StatusSlot.ACTIVE);
      }
    }

    public void ChangeSelected(int i)
    {
       currentActiveSlot.ChangeActive(StatusSlot.NOT_ACTIVE);
      _bottomSlots[i - 1].ChangeActive(StatusSlot.ACTIVE);
      currentActiveSlot = _bottomSlots[i - 1];
    }
    public InventorySlot FindEmptyOrStackSlot(PositionInventory positionInventory, Item item)
    {
      InventorySlot[] slots;
      slots = positionInventory is  PositionInventory.BOTTOM_INVENTORY ? _bottomSlots : _mainSlots;
      InventorySlot slot= null;
      bool ind = false;

      
      foreach (InventorySlot inventorySlot in slots)
      {
        ItemInventory itemInventory = inventorySlot.GetComponentInChildren<ItemInventory>();
        Debug.Log(itemInventory==null);
        Debug.Log(item.Name);
        Debug.Log(ind);
        if (itemInventory == null && !ind)
        {
          
          ind = true;
          slot = inventorySlot;
        }
        Debug.Log("+");
        if (itemInventory!= null &&  itemInventory.Item.IsStackable && itemInventory.Item.Name == item.Name)
        {
          itemInventory.ChangeCount(1);
          return null;
        }
        Debug.Log("-");
      }
      Debug.Log("end");
      return slot;
    }
  }
}