﻿using System;
using UnityEngine;
using UnityEngine.UI;


public enum TypeOfItem
{
  FOOD,
  POISON,
  SWORD,
  ARMOR,
}

namespace Source.Scripts.ScriptableObjects
{
  [CreateAssetMenu(menuName = "Scriptable Object/Item")]
  public class Item: ScriptableObject
  {
    [field: SerializeField] public string Name { get;  set; }
    [field: SerializeField] public Sprite Image { get;  set; }
    [field: SerializeField] public TypeOfItem  Type { get;  set; }
    [field: SerializeField] public bool IsStackable { get;  set; }
    [field: SerializeField] public int Count { get;  set; }
  }
}