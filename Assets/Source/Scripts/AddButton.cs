﻿using System;
using Source.Scripts.ScriptableObjects;
using Source.Scripts.Slots;
using UnityEngine;


namespace Source.Scripts
{
  
  public class AddButton:MonoBehaviour
  {
    [SerializeField] private ItemAssetsStorage _itemAssetsStorage;
    [SerializeField] private ItemMovement _itemMovement;
    [SerializeField] private ItemSpawner _spawner;
    [SerializeField] private UnityEngine.UI.Button _button;
    [SerializeField] private string _name;
    public void Awake()
    {
      _button.onClick.AddListener(AddNewItem);
    }

    private void AddNewItem()
    {
      Debug.Log("AddNewItem " + _name);
      Item itemAsset =  _itemAssetsStorage.GetByName(_name);
      if (_itemAssetsStorage != null)
      {
        Debug.Log("Assert Found " + _name);
        InventorySlot slot = _itemMovement.FindEmptyOrStackSlot(PositionInventory.BOTTOM_INVENTORY,itemAsset);
       
        if (slot != null)
        {
          Debug.Log("Empty Slot Found");
          _spawner.SpawnItem(itemAsset,slot, PositionInventory.BOTTOM_INVENTORY );
        }
      }
    }
  }
}