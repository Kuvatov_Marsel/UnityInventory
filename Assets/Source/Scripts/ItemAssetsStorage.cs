﻿using System.Collections.Generic;
using System.Linq;
using Source.Scripts.ScriptableObjects;
using UnityEngine;

namespace Source.Scripts.Slots
{
  public class ItemAssetsStorage:MonoBehaviour
  {
    private List<Item> _items;

    public void AddItems(List<Item> items)
    {
      _items = items;
      Debug.Log("Load items " + items.Count);
    }

    public Item GetByName(string name)
    {
      return _items.First(x => x.Name == name);
    }
  }
}