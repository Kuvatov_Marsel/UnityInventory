using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Source.Scripts.Slots
{
    public class DragAndDrop : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        [SerializeField] private  Image _image;
        
        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        private Transform _parentAfterDrag;

        public Transform ParentAfterDrag
        {
            get => _parentAfterDrag;
            set => _parentAfterDrag = value;
        }
        public void OnBeginDrag(PointerEventData eventData)
        {
            _image.raycastTarget = false;
       
            _parentAfterDrag = transform.parent;
            transform.SetParent(transform.root);
        }

        public void OnDrag(PointerEventData eventData)
        {
            transform.position = Input.mousePosition;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _image.raycastTarget = true;
            transform.SetParent(_parentAfterDrag);
            
            
        }
    }
}
