﻿using System;
using UnityEngine;
using UnityEngine.UI;

public enum StatusSlot
{
  ACTIVE,
  NOT_ACTIVE,
}

namespace Source.Scripts.Slots
{
  public class InventorySlot:MonoBehaviour
  {
    [SerializeField] private Image _slotImage;
    [SerializeField] private Sprite _notActiveSprite;
    [SerializeField] private Sprite _activeSprite;

    public void ChangeActive(StatusSlot status)
    {
      if (status == StatusSlot.ACTIVE)
      {
        Debug.Log("Active");
        _slotImage.sprite = _activeSprite;
      }
      else
      {
        _slotImage.sprite = _notActiveSprite;
      }
    }
    
  }
}