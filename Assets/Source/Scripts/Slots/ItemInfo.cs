﻿namespace Source.Scripts
{
  public class ItemInfo
  {
     public string Name { get;  set; }
     public TypeOfItem  Type { get;  set; }
     public bool IsStackable { get;  set; }
     public int Count { get;  set; }
  }
}