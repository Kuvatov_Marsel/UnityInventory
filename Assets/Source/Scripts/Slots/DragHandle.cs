﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Source.Scripts.Slots
{
  public class DragHandle:MonoBehaviour, IDropHandler
  {

    public void OnDrop(PointerEventData eventData)
    {
      Debug.Log("DragHandle");
      Debug.Log(eventData.pointerDrag);
      DragAndDrop dragAndDropItem = eventData.pointerDrag.GetComponent<DragAndDrop>();
      if (transform.childCount == 0 && dragAndDropItem!=null)
      {
        
        dragAndDropItem.ParentAfterDrag = transform;
      }
      else
      {
        ItemInventory itemInventory = eventData.pointerDrag.GetComponent<ItemInventory>();
        ItemInventory currentItemInventory = gameObject.GetComponentInChildren<ItemInventory>();
        if (itemInventory != null && dragAndDropItem!=null && currentItemInventory!=null 
            && currentItemInventory.Item.Name == itemInventory.Item.Name && currentItemInventory.Item.IsStackable )
        {
          Debug.Log(itemInventory.Item.Count);
          currentItemInventory.ChangeCount(itemInventory.Item.Count);
          Destroy(itemInventory.gameObject);
          
        }
      }
    }
  }
}