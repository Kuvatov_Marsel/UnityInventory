﻿using Source.Scripts.ScriptableObjects;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum PositionInventory
{
  BOTTOM_INVENTORY,
  MAIN_INVENTORY,
}
namespace Source.Scripts.Slots
{
  public class ItemInventory:MonoBehaviour
  {
    private ItemInfo _item = new ItemInfo();
    private PositionInventory _positionInventory;
    [SerializeField] private Image _image;
    [SerializeField] private TextMeshProUGUI _textCount;
    
    public void Initialize(Item item, PositionInventory positionInventory)
    {
      
      _item.Count = item.Count;
      _item.Name = item.Name;
      _item.IsStackable = item.IsStackable;
      
      _image.sprite = item.Image;
      _positionInventory = positionInventory;
      if (item.IsStackable)
      {
        _textCount.text = item.Count.ToString();
      }
      
    }

    public void ChangeCount(int addCount)
    {
      Debug.Log("Previous Count " + _item.Count);
      Debug.Log("Add Count " + addCount);
      _item.Count+=addCount;
      Debug.Log("New Count " +_item.Count);
      _textCount.text = _item.Count.ToString();
    }
    public ItemInfo Item
    {
      get { return _item; }
      set { _item = value; }
    }
  }
}