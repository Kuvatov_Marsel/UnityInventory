﻿using Source.Scripts.ScriptableObjects;
using UnityEngine;
using UnityEngine.UIElements;

namespace Source.Scripts.Slots
{
  public class ItemSpawner:MonoBehaviour
  {
    [SerializeField] private GameObject itemInventoryPrefab;
    [SerializeField]private ItemsStorage storage;
    public void SpawnItem(Item item, InventorySlot slot, PositionInventory position)
    {
      GameObject newItem = Instantiate(itemInventoryPrefab, slot.transform);
      Debug.Log("Spawning item" + "slot transform" + slot.transform);
      ItemInventory itemInventory = newItem.GetComponent<ItemInventory>();
      itemInventory.Initialize(item,position);
      storage.Add(itemInventory);
    }
  }
}