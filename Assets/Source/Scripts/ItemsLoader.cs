﻿using System.Collections.Generic;
using System.Linq;
using Source.Scripts.ScriptableObjects;
using UnityEngine;

namespace Source.Scripts
{
  public class ItemsLoader:MonoBehaviour
  {
    public List<Item> LoadAllCard(string path)
    {
      List<Item> cardObject = Resources.LoadAll<Item>(path).ToList(); 
      return cardObject;
    }

    public void UnloadCard(Item cardAsset)
    {
      Resources.UnloadAsset(cardAsset);
    }
  }
}