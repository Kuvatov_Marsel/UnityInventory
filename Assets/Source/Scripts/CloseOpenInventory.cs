﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;


public enum ChestStatus
{
  OPEN,
  CLOSED,
}

namespace Source.Scripts
{
  public class CloseOpenInventory:MonoBehaviour, IPointerClickHandler
  {
    [SerializeField] private GameObject _mainInventory;
    [SerializeField] private Image chestImage;
    [SerializeField] private Sprite _closedChest;
    [SerializeField] private Sprite _openChest;
    private ChestStatus _status;
    private void Awake()
    {
      _status = ChestStatus.CLOSED;
      _mainInventory.SetActive(false);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
      if (_status is ChestStatus.OPEN)
      {
        _status = ChestStatus.CLOSED;
        _mainInventory.SetActive(false);
        chestImage.sprite = _closedChest;

      }
      else
      {
        _status = ChestStatus.OPEN;
        _mainInventory.SetActive(true);
        chestImage.sprite = _openChest;
      }
    }
  }
}