﻿using System;
using UnityEngine;

namespace Source.Scripts.Slots
{
  public class GameStarter : MonoBehaviour
  {
    [SerializeField] private ItemsLoader _loader;
    [SerializeField] private ItemSpawner _spawner;
    [SerializeField] private ItemMovement _itemMovement;
    [SerializeField] private ItemAssetsStorage _assetsStorage;
    [SerializeField] private ItemsStorage _itemsStorage;
    private void Awake()
    {
      
      
      StartGame();
    }

    private void StartGame()
    {
      Debug.Log("StartGame");
      _assetsStorage.AddItems(_loader.LoadAllCard("Items"));
      _itemMovement.ActiveFirstSlot();
    }
  }
}