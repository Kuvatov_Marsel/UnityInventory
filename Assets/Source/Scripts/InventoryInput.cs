﻿using System;
using Source.Scripts.Slots;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Source.Scripts
{
  public class InventoryInput:MonoBehaviour
  {
    private InventorySelect _inventorySelect;
    [SerializeField] private ItemMovement _itemMovement;
    private void Awake()
    {
      _inventorySelect = new InventorySelect();
      _inventorySelect.Player.Select.performed += ChangeSelect;

    }

    private void ChangeSelect(InputAction.CallbackContext context)
    {
      int numberCall = int.Parse(context.control.name);
      _itemMovement.ChangeSelected(numberCall);
    }
    private void OnEnable()
    {
      _inventorySelect.Enable();
    }

    private void OnDisable()
    {
      _inventorySelect.Disable();
    }

    private void OnDestroy()
    {
      _inventorySelect.Player.Select.performed -= ChangeSelect;
    }
  }
}