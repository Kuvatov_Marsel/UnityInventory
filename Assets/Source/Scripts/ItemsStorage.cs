﻿using System.Collections.Generic;
using System.Linq;
using Source.Scripts.Slots;
using UnityEngine;

namespace Source.Scripts
{
  public class ItemsStorage: MonoBehaviour
  {
    private List<ItemInventory> _items = new List<ItemInventory>();

    
    public void Add(ItemInventory itemInventory)
    {
      _items.Add(itemInventory);
    }
  }
}